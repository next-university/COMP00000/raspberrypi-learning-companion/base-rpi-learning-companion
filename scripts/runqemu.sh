#!/usr/bin/env bash
#
# Helper script to run qemu

BUILDDIR="$(realpath "$(dirname "$0")/../build")"
DEPLOYDIR="${BUILDDIR}/tmp/deploy/images/qemuarm64"

qemu-system-aarch64 \
	-M virt \
	-cpu cortex-a57 \
	-m 1024 \
	-nographic \
	-hda "${DEPLOYDIR}/rpi-learning-companion-qemuarm64.ext4" \
	-kernel	"${DEPLOYDIR}/Image-qemuarm64.bin" \
	--append "console=ttyAMA0 loglevel=7 rootwait root=/dev/vda" \
