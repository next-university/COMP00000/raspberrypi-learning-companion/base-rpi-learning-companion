DESCRIPTION = "This image contains some mandatory tools"

LICENSE = "MIT"

inherit core-image

IMAGE_INSTALL = "packagegroup-core-boot ${CORE_IMAGE_EXTRA_INSTALL}"
CORE_IMAGE_EXTRA_INSTALL = "\
    ${@bb.utils.contains("MACHINE", "raspberrypi4",	"linux-firmware-rpidistro-bcm43430", "", d)} \
    ${@bb.utils.contains("MACHINE", "raspberrypi4",	"linux-firmware-rpidistro-bcm43455", "", d)} \
    ${@bb.utils.contains("MACHINE", "raspberrypi4",	"bluez-firmware-rpidistro-bcm43430a1-hcd", "", d)} \
    ${@bb.utils.contains("MACHINE", "raspberrypi4",	"bluez-firmware-rpidistro-bcm4345c0-hcd", "", d)} \
    ${@bb.utils.contains("MACHINE", "raspberrypi4",	"kernel-module-brcmfmac", "", d)} \
    kernel-modules \
"
